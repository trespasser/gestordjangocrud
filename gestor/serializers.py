from rest_framework import serializers
from gestor.models import Tarea, Empleado

class TareaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tarea
        fields = "__all__"