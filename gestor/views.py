from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from rest_framework.generics import ListAPIView
from rest_framework.generics import CreateAPIView
from rest_framework.generics import DestroyAPIView
from rest_framework.generics import UpdateAPIView
from gestor.serializers import TareaSerializer
from .models import Empleado, Tarea

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'gestor/index.html'
    context_object_name = 'latest_tarea_list'

    def get_queryset(self):
        return Tarea.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Tarea 
    template_name = 'gestor/detail.html'

# Create your views here.
class ListTareaAPIView(ListAPIView):
    """This endpoint list all of the available todos from the database"""
    queryset = Tarea.objects.all()
    serializer_class = TareaSerializer

class CreateTareaAPIView(CreateAPIView):
    """This endpoint allows for creation of a todo"""
    queryset = Tarea.objects.all()
    serializer_class = TareaSerializer

class UpdateTareaAPIView(UpdateAPIView):
    """This endpoint allows for updating a specific todo by passing in the id of the todo to update"""
    queryset = Tarea.objects.all()
    serializer_class = TareaSerializer

class DeleteTareaAPIView(DestroyAPIView):
    """This endpoint allows for deletion of a specific Todo from the database"""
    queryset = Tarea.objects.all()
    serializer_class = TareaSerializer

