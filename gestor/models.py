import datetime
from django.db import models
from django.utils import timezone
from django.contrib import admin

# Create your models here.

class Tarea(models.Model):
    titulo_text = models.CharField(max_length=200)
    descripcion_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Fecha de creacion')

    def __str__(self):
        return self.titulo_text

    @admin.display(
        boolean=True,
        ordering='pub_date',
        description='Published recently?',
    ) 

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

class Empleado(models.Model):
    tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    nombre_text = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre_text