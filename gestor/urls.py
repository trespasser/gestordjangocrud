from django.urls import path

from . import views

app_name = 'gestor'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path("read/",views.ListTareaAPIView.as_view(),name="tarea_list"),
    path("create/", views.CreateTareaAPIView.as_view(),name="tarea_create"),
    path("update/<int:pk>/",views.UpdateTareaAPIView.as_view(),name="update_tarea"),
    path("delete/<int:pk>/",views.DeleteTareaAPIView.as_view(),name="delete_tarea")
]