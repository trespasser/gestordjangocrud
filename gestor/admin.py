from django.contrib import admin

from .models import Empleado, Tarea

# Register your models here.

class EmpleadoinLine(admin.TabularInline):
    model = Empleado
    extra = 0

class TareaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['titulo_text', 'descripcion_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    #inlines = [EmpleadoinLine]
    list_display = ('titulo_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']

admin.site.register(Tarea, TareaAdmin)